export const CHANGE_PRIMARY_COLOR = "CHANGE_PRIMARY_COLOR";
export const CHANGE_TEXT_CHANGE_COLOR = "CHANGE_TEXT_CHANGE_COLOR";

export const changePrimaryColor = color => ({
  type: CHANGE_PRIMARY_COLOR,
  color
});

export const changeTextChangeColor = () => ({
  type: CHANGE_TEXT_CHANGE_COLOR
});
