export const CHANGE_CURRENCY_AMOUNT = "CHANGE_CURRENCY_AMOUNT";
export const SWAP_CURRENCY = "SWAP_CURRENCY";
export const CHANGE_BASE_CURRENCY = "CHANGE_BASE_CURRENCY";
export const CHANGE_QUOTE_CURRENCY = "CHANGE_QUOTE_CURRENCY";
export const GET_INITIAL_CONVERSION = "GET_INITIAL_CONVERSION";

export const CONVERSION_RESULT = "CONVERSION_RESULT";
export const CONVERSION_ERROR = "CONVERSION_ERROR";

export const GET_BASE_CURRENCY = "GET_BASE_CURRENCY";
export const GET_BASE_CURRENCY_SUCCESS = "GET_BASE_CURRENCY_SUCCESS";
export const GET_BASE_CURRENCY_FAILED = "GET_BASE_CURRENCY_FAILED";

export const getInitialConversion = () => ({
  type: GET_INITIAL_CONVERSION
});
//action creator
export const changeCurrencyAmount = amount => ({
  type: CHANGE_CURRENCY_AMOUNT,
  amount: parseFloat(amount)
});

export const swapCurrency = () => ({
  type: SWAP_CURRENCY
});

export const changeBaseCurrency = currency => ({
  type: CHANGE_BASE_CURRENCY,
  currency
});

export const changeQuoteCurrency = currency => ({
  type: CHANGE_QUOTE_CURRENCY,
  currency
});

export const getBaseCurrency = msg => ({
  type: GET_BASE_CURRENCY,
  msg
});
