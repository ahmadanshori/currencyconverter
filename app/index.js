import React from "react";
import { View } from "react-native";
import EStyleSheet from "react-native-extended-stylesheet";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

import Navigator from "./config/routes";
import { AlertProvider } from "./components/Alert";
import store, { persistor } from "./config/store";

// import NewComponent from "./screens/NewComponent";
// import Todo from "./screens/Todo";
import App from "./components/Todo/App";
// import TodoOri from "./screens/TodoOri";
// import Calculator from "./screens/Calculator";
// import Waktu from "./screens/Timer";
// import Drawing from "./screens/Drawing";
// Component
// import ListViewGrid from "./screens/components/ListViewGrid";
// import SwipeFlatList from "./screens/components/SwipeFlatList";
// import SideDrawer from "./screens/components/SideDrawer";

EStyleSheet.build({
  $primaryBlue: "#4F6D7A",
  $primaryOrange: "#D57A66",
  $primaryGreen: "#00BD9D",
  $primaryPurple: "#9E768F",

  $white: "#FFFFFF",
  $lightGray: "#F0F0F0",
  $border: "#E2E2E2",
  $inputText: "#797979",
  $darkText: "#343434"

  // $outline: 1
});

// export default () => (
//   <Provider store={store}>
//     <PersistGate loading={null} persistor={persistor}>
//       <AlertProvider>
//         <Navigator onNavigationStateChange={null} />
//       </AlertProvider>
//     </PersistGate>
//   </Provider>
// );

// export default () => (
//   <Provider store={store}>
//     <PersistGate loading={null} persistor={persistor}>
//       <Todo />
//     </PersistGate>
//   </Provider>
// );

export default () => <App />;
