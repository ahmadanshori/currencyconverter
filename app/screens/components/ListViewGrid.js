import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableHighlight,
  ListView
} from "react-native";

const THUMB_URLS = [
  require("../../img/1.png"),
  require("../../img/2.png"),
  require("../../img/3.png"),
  require("../../img/4.png"),
  require("../../img/5.png"),
  require("../../img/6.png"),
  require("../../img/7.png"),
  require("../../img/8.png"),
  require("../../img/9.png"),
  require("../../img/10.png"),
  require("../../img/11.png"),
  require("../../img/12.png")
];

class ListViewGrid extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      dataSource: ds.cloneWithRows(this._genRows({}))
    };
  }

  _pressData = {};

  _UNSAFE_componentWillMount() {
    this._pressData = {};
  }

  render() {
    return (
      <ListView
        contentContainerStyle={styles.list}
        dataSource={this.state.dataSource}
        initialListSize={21}
        pageSize={3}
        scrollRenderAheadDistance={500}
        renderRow={this._renderRow}
      />
    );
  }

  _renderRow(rowData, sectionID, rowID) {
    const rowHash = Math.abs(hashCode(rowData));
    const imgSource = THUMB_URLS[rowHash % THUMB_URLS.length];
    return (
      <TouchableHighlight
        onPress={() => this._pressData(rowID)}
        underlayColor="transparent"
      >
        <View>
          <View style={styles.row}>
            <Image style={styles.thumb} source={imgSource} />
            <Text style={styles.text}>{rowData}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  _genRows(pressData) {
    const dataBlob = [];
    for (let ii = 0; ii < 100; ii++) {
      const pressedText = pressData[ii] ? " (x)" : "";
      dataBlob.push(`Cell ${ii}${pressedText}`);
    }
    return dataBlob;
  }

  _pressRow(rowID) {
    this._pressData[rowID] = !this._pressData[rowID];
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(
        this._genRows(this._pressData)
      )
    });
  }
}

var hashCode = function(str) {
  let hash = 15;
  for (let ii = str.length - 1; ii >= 0; ii--) {
    hash = (hash << 5) - hash + str.charCodeAt(ii);
  }
  return hash;
};

const styles = StyleSheet.create({
  list: {
    justifyContent: "space-around",
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start"
  },
  row: {
    justifyContent: "center",
    padding: 5,
    margin: 3,
    width: 100,
    height: 100,
    backgroundColor: "#F6F6F6",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#ccc"
  },
  thumb: {
    width: 64,
    height: 64
  },
  text: {
    flex: 1,
    marginTop: 5,
    fontWeight: "bold"
  }
});

export default ListViewGrid;
