import React, { Component } from "react";
import {
  Image,
  SwipeableFlatList,
  TouchableHighlight,
  StyleSheet,
  Text,
  View,
  Alert
} from "react-native";

const data = [
  {
    key: "like",
    icon: require("../../img/1.png"),
    data: "Like!"
  },
  {
    key: "heart",
    icon: require("../../img/5.png"),
    data: "Heart!"
  },
  {
    key: "party",
    icon: require("../../img/6.png"),
    data: "Party!"
  }
];

class SwipeFlatList extends Component {
  _renderItem = ({ item }) => {
    return (
      <View style={styles.row}>
        <Image style={styles.rowIcon} source={item.icon} />
        <View style={styles.rowData}>
          <Text style={styles.rowDataText}>{item.data}</Text>
        </View>
      </View>
    );
  };
  _renderQuickActions = ({ item }) => {
    return (
      <View style={styles.actionsContainer}>
        <TouchableHighlight
          style={styles.actionButton}
          onPress={() => {
            Alert.alert("Tips", "You could do");
          }}
        >
          <Text style={styles.actionButtonText}>Edit</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={(styles.actionButton, styles.actionButtonDestructive)}
          onPress={() => {
            Alert.alert("tips", "delet disini");
          }}
        >
          <Text style={style.actionButtonText}>Remove</Text>
        </TouchableHighlight>
      </View>
    );
  };
  render() {
    return (
      <View>
        <SwipeableFlatList
          data={data}
          bounceFirstRowOnMount
          maxSwipeDistance={160}
          renderItem={this._renderItem}
          renderQuickAction={this._renderQuickACtions}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    backgroundColor: "#F6F6F6"
  },
  rowIcon: {
    width: 64,
    height: 64,
    marginRight: 20
  },
  rowData: {
    flex: 1
  },
  rowDataText: {
    fontSize: 24
  },
  actionsContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center"
  },
  actionButton: {
    padding: 10,
    width: 80,
    backgroundColor: "#999999"
  },
  actionButtonDestructive: {
    backgroundColor: "#FF0000"
  },
  actionButtonText: {
    textAlign: "center"
  }
});

export default SwipeFlatList;
