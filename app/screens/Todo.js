import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TextInput,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";

import { addTodo, deleteTodo } from "../actions/todos";

class TodoItem extends Component {
  deleteSelf = () => {
    // this.props.dispatch(deleteTodo(this.props.id));
    this.props.onPress(this.props.id);
  };
  render() {
    return (
      <TouchableOpacity onPress={this.deleteSelf}>
        <View style={styles.todoContainer}>
          <Text style={styles.todoText}>{this.props.text}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

class Todo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newTodoText: ""
    };
  }

  addNewTodo = () => {
    const { newTodoText } = this.state;
    if (newTodoText && newTodoText != "") {
      this.setState({
        newTodoText: ""
      });
      this.props.dispatch(addTodo(newTodoText));
    }
  };
  handleTodoText = text => {
    this.setState({
      newTodoText: text
    });
  };

  deleteItem = id => {
    this.props.dispatch(deleteTodo(id));
  };

  renderTodos = () => {
    return this.props.todos.map(todo => {
      return (
        <TodoItem
          onPress={this.deleteItem}
          text={todo.text}
          key={todo.id}
          id={todo.id}
        />
      );
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor="#2ecc71" />
        <View style={styles.topBar}>
          <Text style={styles.title}>To-Do List</Text>
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            onChangeText={this.handleTodoText}
            value={this.state.newTodoText}
            returnKeyType="done"
            placeholder="Todo"
            onSubmitEditing={this.addNewTodo}
            style={styles.input}
          />
        </View>
        <ScrollView automaticallyAdjustContentInsets={false}>
          {this.renderTodos()}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "stretch"
  },
  topBar: {
    padding: 10,
    paddingTop: 10,
    paddingBottom: 8,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#2ecc71"
  },
  title: {
    color: "white",
    fontSize: 20
  },
  inputContainer: {
    padding: 8,
    paddingTop: 0,
    backgroundColor: "#2ecc71"
  },
  input: {
    height: 26,
    padding: 4,
    paddingLeft: 8,
    borderRadius: 8,
    backgroundColor: "white"
  },
  todoContainer: {
    padding: 16,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    marginBottom: 0,
    borderColor: "#ccc",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  }
});

const mapStateToProps = state => {
  return {
    todos: state.todos
  };
};

export default connect(mapStateToProps)(Todo);
