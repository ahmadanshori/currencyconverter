import PropTypes from "prop-types";
import React, { Component } from "react";
import { ScrollView, StatusBar, Platform, Linking } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { ListItem, Separator } from "../components/List";
import { connectAlert } from "../components/Alert";

const ICON_PREFIX = Platform.OS === "ios" ? "ios" : "md";
const ICON_COLOR = "#868686";
const ICON_SIZE = 23;

class Options extends Component {
  static propTypes = {
    navigation: PropTypes.object,
    alertWithType: PropTypes.func
  };

  handlePressThemes = () => {
    const { navigation } = this.props;
    navigation.navigate("Themes");
  };

  handlePressSite = () => {
    const { alertWithType } = this.props;
    Linking.openURL("https://ahmadanshori.gitlab.io/portfolio/").catch(() =>
      alertWithType("error", "Sorry!", "can't be opened right now.")
    );
  };
  newComponent = () => {
    const { navigation } = this.props;
    navigation.navigate("NewComponent");
  };

  calculator = () => {
    const { navigation } = this.props;
    navigation.navigate("Calculator");
  };

  timer = () => {
    const { navigation } = this.props;
    navigation.navigate("Timer");
  };

  handlerTodo = () => {
    const { navigation } = this.props;
    navigation.navigate("Todo");
  };

  render() {
    return (
      <ScrollView>
        <StatusBar translucent={false} barStyle="default" />
        <ListItem
          text="Themes"
          onPress={this.handlePressThemes}
          customIcon={
            <Icon
              name={`${ICON_PREFIX}-arrow-forward`}
              size={ICON_SIZE}
              color={ICON_COLOR}
            />
          }
        />
        <Separator />
        <ListItem
          text="Ahmad Anshori"
          onPress={this.handlePressSite}
          customIcon={
            <Icon
              name={`${ICON_PREFIX}-link`}
              size={ICON_SIZE}
              color={ICON_COLOR}
            />
          }
        />
        <Separator />
        <ListItem
          text="Test Component"
          onPress={this.newComponent}
          customIcon={
            <Icon
              name={`${ICON_PREFIX}-bonfire`}
              size={ICON_SIZE}
              color={ICON_COLOR}
            />
          }
        />
        <Separator />
        <ListItem
          text="Calculator"
          onPress={this.calculator}
          customIcon={
            <Icon name={"md-calculator"} size={ICON_SIZE} color={ICON_COLOR} />
          }
        />
        <Separator />
        <ListItem
          text="Timer"
          onPress={this.timer}
          customIcon={
            <Icon
              name={`${ICON_PREFIX}-timer`}
              size={ICON_SIZE}
              color={ICON_COLOR}
            />
          }
        />
        <Separator />
        <ListItem
          text="Todo"
          onPress={this.handlerTodo}
          customIcon={
            <Icon
              name={`${ICON_PREFIX}-appstore`}
              size={ICON_SIZE}
              color={ICON_COLOR}
            />
          }
        />
        <Separator />
      </ScrollView>
    );
  }
}
export default connectAlert(Options);
