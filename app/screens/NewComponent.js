import React, { Component } from "react";
import {
  View,
  Dimensions,
  StatusBar,
  Button,
  StyleSheet,
  Animated,
  Image,
  Easing
} from "react-native";

export default class NewComponent extends Component {
  constructor() {
    super();
    this.spinValue = new Animated.Value(0);
  }
  componentDidMount() {
    this.spin();
  }
  spin = () => {
    this.spinValue.setValue(0);
    Animated.timing(this.spinValue, {
      toValue: 1,
      duration: 4000,
      easing: Easing.linear
    }).start(() => this.spin());
  };
  
  render() {
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ["0deg", "360deg"]
    });
    return (
      <View>
        <View style={styles.satu}>
          <Animated.Image
            style={{
              width: 227,
              height: 200,
              transform: [{ rotate: spin }]
            }}
            source={{
              uri:
                "https://s3.amazonaws.com/media-p.slid.es/uploads/alexanderfarennikov/images/1198519/reactjs.png"
            }}
          />
        </View>
        <View style={styles.dua}>
          <Animated.Image
            style={{
              width: 227,
              height: 200,
              transform: [{ rotate: spin }]
            }}
            source={{
              uri:
                "https://s3.amazonaws.com/media-p.slid.es/uploads/alexanderfarennikov/images/1198519/reactjs.png"
            }}
          />
        </View>
        <View style={styles.container}>
          <Animated.Image
            style={{
              width: 227,
              height: 200,
              transform: [{ rotate: spin }]
            }}
            source={{
              uri:
                "https://s3.amazonaws.com/media-p.slid.es/uploads/alexanderfarennikov/images/1198519/reactjs.png"
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get("window").width / 3,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "red"
  },
  satu: {
    width: Dimensions.get("window").width / 3,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "blue"
  },
  dua: {
    width: Dimensions.get("window").width / 3,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "orange"
  }
});
