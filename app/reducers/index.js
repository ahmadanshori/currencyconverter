import { combineReducers } from "redux";

import currencies from "./currencies";
import theme from "./theme";
import todos from "./todos";

export default combineReducers({
  currencies,
  theme,
  todos
});
