import uuid from "uuid";
import { ADD_TODO, DELETE_TODO } from "../actions/todos";

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return [
        ...state,
        {
          text: action.text,
          id: uuid.v4()
        }
      ];
    case DELETE_TODO:
      var newTodos = state.filter(todo => {
        if (todo.id == action.id) {
          return false;
        }
        return true;
      });
      return newTodos;
    default:
      return state;
  }
};
