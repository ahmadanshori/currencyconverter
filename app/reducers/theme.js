import {
  CHANGE_PRIMARY_COLOR,
  CHANGE_TEXT_CHANGE_COLOR
} from "../actions/theme";

const initialState = {
  primaryColor: "#4F6D7A"
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_PRIMARY_COLOR:
      return { ...state, primaryColor: action.color };
    case CHANGE_TEXT_CHANGE_COLOR:
      return { ...state, primaryColor: action.color };
    default:
      return state;
  }
};
