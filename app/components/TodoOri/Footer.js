import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { colorPrimary, colorSecondary } from "./styles/commonStyles";

export default class Footer extends Component {
  render() {
    return (
      <View
        style={[
          styles.component3,
          { backgroundColor: this.props.primaryColor }
        ]}
      >
        <Text style={styles.welcome}>Ahmad Anshori</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  component3: {
    //flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    //alignSelf: 'stretch',
    height: 50
    //backgroundColor: colorPrimary,
  }
});
