import EStyleSheet from "react-native-extended-stylesheet";

export default EStyleSheet.create({
  container: {
    alignItems: "center"
  },
  wrapper: {
    flexDirection: "row",
    alignItems: "center"
  },
  icon: {
    width: 19,
    marginRight: 11
  },
  text: {
    color: "$white",
    fontSize: 14,
    paddingVertical: 20,
    fontWeight: "300"
  },

  // Calculator
  rootContainer: {
    flex: 1
  },
  displayContainer: {
    flex: 2,
    justifyContent: "center",
    backgroundColor: "#193441"
  },
  displayText: {
    color: "white",
    fontSize: 38,
    fontWeight: "bold",
    textAlign: "right",
    padding: 20
  },
  inputContainer: {
    flex: 8,
    backgroundColor: "#3E606F"
  },
  inputButton: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.5,
    borderColor: "#91AA9D"
  },
  inputButtonHighlighted: {
    backgroundColor: "#193441"
  },
  inputButtonText: {
    fontSize: 22,
    fontWeight: "bold",
    color: "white"
  },
  inputRow: {
    flex: 1,
    flexDirection: "row"
  }
});
