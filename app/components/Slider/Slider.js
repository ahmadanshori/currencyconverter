import React, { Component } from "react";
import { Slider, Text, StyleSheet, View } from "react-native";

class Slider extends Component {
  static defaultProps = {
    value: 0
  };

  state = {
    value: this.props.value
  };

  render() {
    return (
      <View>
        <Text style={styles.text} >
            {this.state.value && this.state.value.toFixed(3)}
        </Text>
        <Slider {...this.props} onValueChange={value => this.setState({})/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  slider: {
    height: 10,
    margin
  },
  text: {
    fontSize: 14,
    textAlign: "center",
    fontWeight: "500",
    margin: 10
  }
});

export default Slider;
