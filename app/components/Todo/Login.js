import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { TextInput } from "react-native-gesture-handler";

class Login extends Component {
  rendert() {
    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Ori</Text>
        </View>
        <View style={styles.field}>
          <TextInput placeholder="Email" style={styles.textInput} />
        </View>
        <View style={styles.field}>
          <TextInput placeholder="Password" style={styles.textInput} />
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.button}>
            <Text>Sign In</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}>
            <Text>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "streetch",
    paddingTop: 20,
    backgroundColor: "#aaa"
  },
  titleContainer: {
    padding: 10
  },
  title: {
    color: "white",
    fontSize: 35
  },
  field: {
    borderRadius: 5,
    padding: 5,
    paddingLeft: 8,
    margin: 7,
    marginTop: 0,
    backgroundColor: "white"
  },
  textInput: {
    height: 26
  },
  buttonContainer: {
    padding: 20,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  button: {
    fonstSize: 30,
    color: "white"
  }
});

export default Login;
