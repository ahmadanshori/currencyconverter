import React from "react";
import { Text } from "react-native";
import PropTypes from "prop-types";

class TestComponent extends React.Component {
  constructor(props) {
    super(props);
    console.log("constructor");
  }

  state = {
    number: 0
  };

  componentWillMount() {
    console.log("componentWillMount");
  }

  componentDidMount() {
    console.log("componentDidMount");
  }

  componentWillReceiveProps(nextProps) {
    console.log("componentWillReceiveProps", nextProps);
  }

  //   shouldComponentUpdate() {
  //     console.log("shouldComponentUpdate");

  //     if (this.state.number >= 10 && this.state.number <= 15) {
  //       return false;
  //     } else {
  //       return true;
  //     }
  //   }

  componentWillUpdate(nextProps, nextState) {
    console.log("componentWillUpdate");
  }

  componentDidUpdate() {
    console.log("componentDidUpdate");
  }

  componentWillUnmount() {
    console.log(this.props.text);
    console.log("componentWillUnmount");
  }

  onPress = () => {
    this.setState({
      number: this.state.number + 1
    });
  };
  render() {
    console.log("render");
    return (
      <Text onPress={this.onPress} style={this.props.style || {}}>
        Hello {this.props.title}
      </Text>
    );
  }
}

TestComponent.propTypes = {
  title: PropTypes.string.isRequired,
  style: PropTypes.object,
  onPress: PropTypes.func.isRequired
};

export default TestComponent;
