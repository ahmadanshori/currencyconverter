import { StatusBar } from "react-native";
import { createStackNavigator } from "react-navigation";

import Home from "../screens/Home";
import CurrencyList from "../screens/CurrencyList";
import Options from "../screens/Options";
import Themes from "../screens/Themes";
import NewComponent from "../screens/NewComponent";
import Calculator from "../screens/Calculator";
import Timer from "../screens/Timer";
import Todo from "../screens/Todo";

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        header: () => null,
        headerTitle: "Home"
      }
    },
    Options: {
      screen: Options,
      navigationOptions: {
        headerTitle: "Options"
      }
    },
    Themes: {
      screen: Themes,
      navigationOptions: {
        headerTitle: "Themes"
      }
    }
  },
  {
    headerMode: "screen"
  }
);

const CurrencyListStack = createStackNavigator({
  CurrencyList: {
    screen: CurrencyList,
    navigationOptions: ({ navigation }) => ({
      headerTitle: navigation.state.params.title
    })
  }
});

const NewComponentStack = createStackNavigator({
  NewComponent: {
    screen: NewComponent,
    navigationOptions: {
      header: () => null,
      headerTitle: "New Components"
    }
  }
});

const CalculatorStack = createStackNavigator({
  Calculator: {
    screen: Calculator,
    navigationOptions: {
      header: () => null,
      headerTitle: "Calculator"
    }
  }
});

const TimerStack = createStackNavigator({
  Timer: {
    screen: Timer,
    navigationOptions: {
      header: () => null,
      headerTitle: "Timer"
    }
  }
});

const Todostack = createStackNavigator({
  Todo: {
    screen: Todo,
    navigationOptions: {
      header: () => null,
      headerTitle: "Todo"
    }
  }
});

export default createStackNavigator(
  {
    Home: {
      screen: HomeStack
    },
    CurrencyList: {
      screen: CurrencyListStack
    },
    NewComponent: {
      screen: NewComponentStack
    },
    Calculator: {
      screen: CalculatorStack
    },
    Timer: {
      screen: TimerStack
    },
    Todo: {
      screen: Todostack
    }
  },
  {
    mode: "modal",
    headerMode: "none"
  }
);
