import { takeEvery, call, put, select } from "redux-saga/effects";

import {
  CHANGE_BASE_CURRENCY,
  GET_INITIAL_CONVERSION,
  SWAP_CURRENCY,
  CONVERSION_RESULT,
  CONVERSION_ERROR,
  GET_BASE_CURRENCY,
  GET_BASE_CURRENCY_SUCCESS,
  GET_BASE_CURRENCY_FAILED
} from "../actions/currencies";

export const getLatestRate = currency =>
  fetch(`http://fixer.handlebarlabs.com/latest?base=${currency}`);
// https://data.fixer.io/api/latest?access_key=62e8fb998bd30c0754a216631bc2fdb8

const fetchLatestConversionRates = function*(action) {
  try {
    console.log(action);
    let { currency } = action;
    if (currency === undefined) {
      currency = yield select(state => state.currencies.baseCurrency);
    }
    const response = yield call(getLatestRate, currency);
    const result = yield response.json();
    if (result.error) {
      yield put({ type: CONVERSION_ERROR, error: result.error });
    } else {
      yield put({ type: CONVERSION_RESULT, result });
    }
  } catch (error) {
    yield put({ type: CONVERSION_ERROR, error: error.message });
  }
};

const getBaseCurrency = function*({ id }) {
  try {
    const response = yield call(getMsg, 2);

    yield put({
      type: GET_BASE_CURRENCY_SUCCESS,
      msg: `${currencies.baseCurrency} ${msg}`
    });
  } catch (error) {
    yield put({ type: GET_BASE_CURRENCY_FAILED, error: "ERROR" });
  }
};

function getMsg(int) {
  let a = 1 + int;
  return a;
}

const rootSaga = function*() {
  yield takeEvery(GET_INITIAL_CONVERSION, fetchLatestConversionRates);
  yield takeEvery(CHANGE_BASE_CURRENCY, fetchLatestConversionRates);
  yield takeEvery(SWAP_CURRENCY, fetchLatestConversionRates);
  yield takeEvery(GET_BASE_CURRENCY, getBaseCurrency);
};

export default rootSaga;
