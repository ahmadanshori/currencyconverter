import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import { AsyncStorage } from "react-native";
import { persistStore, persistReducer } from "redux-persist";
import createSagaMiddleware from "redux-saga";
import storage from "redux-persist/lib/storage";

import reducer from "../reducers";
import rootSaga from "./sagas";

const persistConfig = {
  key: "root",
  storage
  // blacklist: ["theme"]
};
// redux-persis = untuk menyimpan state kedalam memory hp, di
const persistedReducer = persistReducer(persistConfig, reducer);

const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];
if (process.env.NODE_ENV === "development") {
  middleware.push(logger);
}

let store = createStore(persistedReducer, applyMiddleware(...middleware));
let persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export default store;
export { persistor };
